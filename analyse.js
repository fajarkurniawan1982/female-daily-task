const scanner = require("sonarqube-scanner");

scanner(
  {
    // this example uses local instance of SQ
    serverUrl: "http://52.220.107.219:8800/",
    options: {
      "sonar.login":"1167297752c0cdb5e84d0592e1241f564b4a9ef7",
      "sonar.projectVersion": "1.1.0",
      "sonar.sources": "./",
      //"sonar.tests": "tests",
      "sonar.javascript.lcov.reportPaths": "coverage/lcov.info",
      //"sonar.testExecutionReportPaths": "coverage"
    },
  },
  () => {
    // callback is required
  }
);
