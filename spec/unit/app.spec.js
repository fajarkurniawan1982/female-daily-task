var myapp = require("../../app");
var supertest = require("supertest");
var server = supertest(myapp);

describe("Run Unit test",function(){

    it("Should return 200 healthcheck endpoint to make sure apps is running",function(done){
        server
        .get("/")
        .set('Accept', 'application/json')
        .expect(200, done);
      });

    it("Should return 200 get client IP endpoint",function(done){
        server
        .get("/endpoint?whoami=1")
        .set('Accept', 'application/json')
        .expect(200, done)
      });

    it("Should return 200  get current time endpoint",function(done){
        server
        .get("/endpoint?time=now")
        .set('Accept', 'application/json')
        .expect(200, done)
        
      });


    //404 EXAMPLE
      it("should return 404",function(done){
        server
        .get("/random")
        .expect(404, done)
        
      });


    // BAD REQUEST EXAMPLE
    it("Should return 400 bad request",function(done){
        server
        .get('/endpoint?whoami=xx')
        .set('Accept', 'application/json')
        .expect(400, done)
      });



});
