let express = require("express");
let app = express();
let bodyParser = require("body-parser");

app.disable("x-powered-by");
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

var router = express.Router();

app.use('/',router);

router.get('/',function(request,response){
  response.json({"error" : false, "message" : "Hello !"});
});



router.get('/endpoint', function(request, response) {
     
      var whoami = request.query.whoami;
      var time = request.query.time;
      var date = new Date().toISOString().replace('T', ' ').substring(0, 19);
     
     //var date = new Date().toISOString().replace('T', ' ').substring(0, 19).getTime() + new Date().getTimezoneOffset();

     if(whoami == 1){

         const ip = request.ip;
         response.json({"error" : false, "message" : "Your IP is " + ip});

     } else if(time == 'now'){

         response.json({"error": false, "message":"Current time is "+ date});

     } else {
        response.status(400).json({"error": true, "message": "Do Nothing"});
     }
 
 });

app.listen(3000);
module.exports = app;
console.log("node express app started at http://localhost:3000");
